package org.xiaoweige.mybatis.cipher.example.service.user;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.xiaoweige.mybatis.annotation.EnableCipher;
import org.xiaoweige.mybatis.cipher.example.model.po.User;

import java.util.ArrayList;
import java.util.List;

/**
 * UserService
 * @author Jerry.hu
 * @summary UserService
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description UserService
 * @since 2018-06-07 14:07
 */
@Mapper
public interface UserMapper {

    @EnableCipher(EnableCipher.CipherType.ENCRYPT)
    int singleInsertAndReturnId(User user);

    @EnableCipher(value = EnableCipher.CipherType.ENCRYPT,EnableBatch = true)
    int singleInsert(@Param(value = "xxx") User user);

    @EnableCipher(EnableCipher.CipherType.DECRYPT)
    User findById(long id);

    @EnableCipher(value = EnableCipher.CipherType.DECRYPT,EnableBatch = true)
    List<User> findList();

    @EnableCipher(value = EnableCipher.CipherType.ENCRYPT,EnableBatch = true)
    void insertList(@Param("parama") ArrayList<User> asList);
}
