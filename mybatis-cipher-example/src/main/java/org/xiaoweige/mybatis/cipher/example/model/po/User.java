package org.xiaoweige.mybatis.cipher.example.model.po;

import java.io.Serializable;
import lombok.Data;
import org.xiaoweige.mybatis.annotation.Encrypted;

/**
 * User
 * @author Jerry.hu
 * @summary User
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description User
 * @since 2018-06-07 13:47
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1085358173887328449L;
    /**
     * 用户id
     */
    private Long id;
    
    /**
     * 用户名称
     */
    private String name;
    /**
     * 手机号码 需要加密
     */
    @Encrypted
    private String mobile;
    /**
     * 账户余额
     */
    @Encrypted
    private String account;


    public static void main(String[] args) {
        System.out.println("人".length());
    }


    /**
     * 获取 用户id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * 设置 用户id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取 用户名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置 用户名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取 手机号码 需要加密
     */
    public String getMobile() {
        return this.mobile;
    }

    /**
     * 设置 手机号码 需要加密
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取 账户余额
     */
    public String getAccount() {
        return this.account;
    }

    /**
     * 设置 账户余额
     */
    public void setAccount(String account) {
        this.account = account;
    }
}
