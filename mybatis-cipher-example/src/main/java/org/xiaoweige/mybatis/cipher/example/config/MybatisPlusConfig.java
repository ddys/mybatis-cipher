package org.xiaoweige.mybatis.cipher.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.xiaoweige.mybatis.interceptor.FieldEncryptInterceptor;

/**
 * MybatisPlusConfig
 * @author Jerry.hu
 * @summary MybatisPlusConfig
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description MybatisPlusConfig
 * @since 2018-09-29 16:46
 */
@EnableTransactionManagement
@Configuration
public class MybatisPlusConfig {

    @Bean
    public FieldEncryptInterceptor paginationInterceptor() {
        return new FieldEncryptInterceptor();
    }
}
