package org.xiaoweige.mybatis.annotation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.xiaoweige.mybatis.config.BeanConfig;
import org.xiaoweige.mybatis.config.BeanFactoryHolder;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * encrypted
 * @author Jerry.hu
 * @summary encrypted
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description encrypted
 * @since 2018-06-10 15:11
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(BeanConfig.class)
public @interface EnableEncrypt {
}
