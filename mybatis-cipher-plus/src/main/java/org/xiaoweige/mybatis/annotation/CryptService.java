package org.xiaoweige.mybatis.annotation;

import java.util.List;
import java.util.Map;

/**
 * 加解密服务接口
 * @author Jerry.hu
 * @summary 加解密服务接口
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description 加解密服务接口
 * @since 2018-09-29 10:58:18
 */
public interface CryptService {
    /**
     * 单条加密
     * @author Jerry.hu
     * @modifier Jerry.hu
     * @since 2018-09-29 10:58:18
     * @param value 待加密字段
     * @return 加密后的字符串
     */
    String encrypt(String value);

    /**
     * 单条解密
     * @author Jerry.hu
     * @modifier Jerry.hu
     * @since 2018-09-29 10:58:18
     * @param value 待解密字段
     * @return  解密后的字符串
     */
    String decrypt(String value);

    /**
     * 批量解密
     * @author Jerry.hu
     * @modifier Jerry.hu
     * @since 2018-09-29 10:58:18
     * @param ori 待解密密文集合
     * @return map key 密文 value 明文
     */
    Map<String,String> batchDecrypt(List<String> ori);

    /**
     * 批量加密
     * @author Jerry.hu
     * @modifier Jerry.hu
     * @since 2018-09-29 10:59:36
     * @param ori 待解密密文集合
     * @return map key 明文  value 原密
     */
    Map<String,String> batchEncrypt(List<String> ori);

}
